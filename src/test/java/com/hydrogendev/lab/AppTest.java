package com.hydrogendev.lab;

import com.hydrogendev.lab.controller.QuestionController;
import com.hydrogendev.lab.entity.Choice;
import com.hydrogendev.lab.entity.Question;
import junitparams.JUnitParamsRunner;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;


@RunWith(JUnitParamsRunner.class)
public class AppTest {

    @Test
    public void testApp() throws Exception {
        QuestionController controller = new QuestionController();
        //      Question 1
        List<Choice> q1ChoiceList = new ArrayList<>();
        Choice q1choice1 = new Choice(1,"Primitive",false);
        Choice q1choice2 = new Choice(2,"Reference",true);
        q1ChoiceList.add(q1choice1);
        q1ChoiceList.add(q1choice2);
        Question question1 = new Question(
                1,
                "Primitive or Reference??",
                "Java has 2 data types, What is the type of String?",
                q1ChoiceList,
                10
        );
        controller.addQuestion(question1);
//      Question 2
        List<Choice> q2ChoiceList = new ArrayList<>();
        Choice q2Choice1 = new Choice(1,"1",false);
        Choice q2Choice2 = new Choice(2,"9",false);
        Choice q2Choice3 = new Choice(3,"Syntax error", true);
        q2ChoiceList.add(q2Choice1);
        q2ChoiceList.add(q2Choice2);
        q2ChoiceList.add(q2Choice3);
        Question question2 = new Question(
                2,
                "Can you calculate this using Java?",
                " 6/2(1+2) what is the answer of this?",
                q2ChoiceList,
                10
        );
        controller.addQuestion(question2);
//      Question 3
        List<Choice> q3ChoiceList = new ArrayList<>();
        Choice q3Choice1 = new Choice(1,"Pizza",false);
        Choice q3Choice2 = new Choice(2,"Dinner",true);
        Choice q3Choice3 = new Choice(3,"Pudding", false);
        q3ChoiceList.add(q3Choice1);
        q3ChoiceList.add(q3Choice2);
        q3ChoiceList.add(q3Choice3);
        Question question3 = new Question(
                3,
                "Tricky question",
                " What can you never eat before breakfast?",
                q3ChoiceList,
                10
        );
        controller.addQuestion(question3);

        assertThat(controller.nextQuestion().getId(),is(1));
        controller.answer(1);
        assertThat(controller.checkScore(),is(0));

        assertThat(controller.nextQuestion().getId(),is(2));
        controller.answer(3);
        assertThat(controller.checkScore(),is(10));

        assertThat(controller.nextQuestion().getId(),is(3));
        controller.answer(2);
        assertThat(controller.checkScore(),is(20));

        assertThat(controller.nextQuestion().getId(),is(1));
        controller.answer(2);
        assertThat(controller.checkScore(),is(30));

    }
}
