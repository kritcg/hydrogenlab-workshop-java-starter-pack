package com.hydrogendev.lab.controller;

import com.hydrogendev.lab.entity.Choice;
import com.hydrogendev.lab.entity.Question;
import junitparams.JUnitParamsRunner;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;


@RunWith(JUnitParamsRunner.class)
public class QuestionControllerTest {

    @Test
    public void testNextQuestion() throws Exception {
//      Question 1
        List<Choice> q1ChoiceList = new ArrayList<>();
        Choice q1choice1 = new Choice(1,"Primitive",false);
        Choice q1choice2 = new Choice(2,"Reference",true);
        q1ChoiceList.add(q1choice1);
        q1ChoiceList.add(q1choice2);
        Question question1 = new Question(
                1,
                "Primitive or Reference??",
                "Java has 2 data types, What is the type of String?",
                q1ChoiceList,
                10
        );
//      Question 2
        List<Choice> q2ChoiceList = new ArrayList<>();
        Choice q2Choice1 = new Choice(1,"1",false);
        Choice q2Choice2 = new Choice(2,"9",false);
        Choice q2Choice3 = new Choice(3,"Syntax error", true);
        q2ChoiceList.add(q2Choice1);
        q2ChoiceList.add(q2Choice2);
        q2ChoiceList.add(q2Choice3);
        Question question2 = new Question(
                2,
                "Can you calculate this using Java?",
                " 6/2(1+2) what is the answer of this?",
                q2ChoiceList,
                10
        );
        List<Question> questionList = new ArrayList<>();

        questionList.add(question1);
        questionList.add(question2);

        QuestionController controller = new QuestionController(
                -1,
                questionList,
                0,
                new HashMap<Integer, Integer>()
        );

        assertThat(controller.nextQuestion(),is(question1));
        assertThat(controller.nextQuestion(),is(question2));
        assertThat(controller.nextQuestion(),is(question1));
    }

    @Test
    public void testAddQuestion() throws Exception {
//      Question 1
        List<Choice> q1ChoiceList = new ArrayList<>();
        Choice q1choice1 = new Choice(1,"Primitive",false);
        Choice q1choice2 = new Choice(2,"Reference",true);
        q1ChoiceList.add(q1choice1);
        q1ChoiceList.add(q1choice2);
        Question question1 = new Question(
                1,
                "Primitive or Reference??",
                "Java has 2 data types, What is the type of String?",
                q1ChoiceList,
                10
        );
//      Question 2
        List<Choice> q2ChoiceList = new ArrayList<>();
        Choice q2Choice1 = new Choice(1,"1",false);
        Choice q2Choice2 = new Choice(2,"9",false);
        Choice q2Choice3 = new Choice(3,"Syntax error", true);
        q2ChoiceList.add(q2Choice1);
        q2ChoiceList.add(q2Choice2);
        q2ChoiceList.add(q2Choice3);
        Question question2 = new Question(
                2,
                "Can you calculate this using Java?",
                " 6/2(1+2) what is the answer of this?",
                q2ChoiceList,
                10
        );
        List<Question> questionList = new ArrayList<>();

        QuestionController controller = new QuestionController(
                -1,
                questionList,
                0,
                new HashMap<Integer, Integer>()
        );

        controller.addQuestion(question1);
        controller.addQuestion(question2);
        assertThat(controller.nextQuestion(),is(question1));
        assertThat(controller.nextQuestion(),is(question2));
    }

    @Test
    public void testPreviousQuestion() throws Exception {

    }
}
