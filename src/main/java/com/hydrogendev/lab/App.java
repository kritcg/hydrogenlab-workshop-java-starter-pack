package com.hydrogendev.lab;

import com.hydrogendev.lab.controller.QuestionController;
import com.hydrogendev.lab.entity.Choice;
import com.hydrogendev.lab.entity.Question;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Hydrogen Dev!
 * Happy workshop XD
 */
public class App 
{
    public static void main( String[] args )
    {
        Scanner scanner = new Scanner(System.in);
        QuestionController controller = new QuestionController();
        addQuestionToController(controller);
        String stopFlag = "x";
        String key = "";

        System.out.println("Welcome to Question Fight~");
        System.out.println("Press ["+stopFlag+"] to exit");
        while (!key.equals(stopFlag)){
            Question q = controller.nextQuestion();

            System.out.println("Question id:"+q.getId());
            System.out.println(q.getTitle());
            System.out.println("Description:"+q.getDescription());
            System.out.println("Question Score:"+q.getScore());
            for (Choice choice : q.getChoices()) {
                System.out.println("\t"+choice.getId()+":"+choice.getContent());
            }
            System.out.println("Press choice id to answer");
            key = scanner.nextLine();
            if(isNumber(key))
                controller.answer(Integer.parseInt(key));
            else if (key.equals(stopFlag))
                break;
            else{
                controller.previousQuestion();
                System.out.println("Please try again");
            }

        }
        System.out.println("Your score is "+controller.checkScore()+"/"+controller.getMaxScore());
        System.out.println("Good bye~");


    }
    public static void addQuestionToController(QuestionController controller){
//      Question 1
        List<Choice> q1ChoiceList = new ArrayList<>();
        Choice q1choice1 = new Choice(1,"Primitive",false);
        Choice q1choice2 = new Choice(2,"Reference",true);
        q1ChoiceList.add(q1choice1);
        q1ChoiceList.add(q1choice2);
        Question question1 = new Question(
            1,
            "Primitive or Reference??",
            "Java has 2 data types, What is the type of String?",
            q1ChoiceList,
            10
        );
        controller.addQuestion(question1);
//      Question 2
        List<Choice> q2ChoiceList = new ArrayList<>();
        Choice q2Choice1 = new Choice(1,"1",false);
        Choice q2Choice2 = new Choice(2,"9",false);
        Choice q2Choice3 = new Choice(3,"Syntax error", true);
        q2ChoiceList.add(q2Choice1);
        q2ChoiceList.add(q2Choice2);
        q2ChoiceList.add(q2Choice3);
        Question question2 = new Question(
                2,
                "Can you calculate this using Java?",
                " 6/2(1+2) what is the answer of this?",
                q2ChoiceList,
                10
        );
        controller.addQuestion(question2);
//      Question 3
        List<Choice> q3ChoiceList = new ArrayList<>();
        Choice q3Choice1 = new Choice(1,"Pizza",false);
        Choice q3Choice2 = new Choice(2,"Dinner",true);
        Choice q3Choice3 = new Choice(3,"Pudding", false);
        q3ChoiceList.add(q3Choice1);
        q3ChoiceList.add(q3Choice2);
        q3ChoiceList.add(q3Choice3);
        Question question3 = new Question(
                3,
                "Tricky question",
                " What can you never eat before breakfast?",
                q3ChoiceList,
                10
        );
        controller.addQuestion(question3);
    }

    public static boolean isNumber(String s){
        try{
            int temp = Integer.parseInt(s);
            return true;
        }
        catch (NumberFormatException e){
            return false;
        }
    }
}
