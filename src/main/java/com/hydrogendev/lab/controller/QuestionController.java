package com.hydrogendev.lab.controller;

import com.hydrogendev.lab.entity.Choice;
import com.hydrogendev.lab.entity.Question;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Chertpong on 18/1/2559.
 */
public class QuestionController {
    private int currentQuestionIndex;
    private List<Question> questionList;
    private int maxScore;
    private Map<Integer,Integer> answerList;

    public QuestionController() {
        // current index = -1 so, nextQuestion will start on first question (index 0)
        this.currentQuestionIndex = -1;
        this.questionList = new ArrayList<Question>();
        this.maxScore = 0;
        this.answerList = new HashMap<Integer, Integer>();
    }

    public QuestionController(int currentQuestionIndex, List<Question> questionList, int maxScore, Map<Integer, Integer> answerList) {
        this.currentQuestionIndex = currentQuestionIndex;
        this.questionList = questionList;
        this.maxScore = maxScore;
        this.answerList = answerList;
    }

    public Question nextQuestion(){
        currentQuestionIndex++;
        Question question = questionList.get(currentQuestionIndex % questionList.size());
        return question;
    }

    public Question previousQuestion(){
        if(currentQuestionIndex <= 0)
            currentQuestionIndex = questionList.size()-1;
        else
            currentQuestionIndex -= 1;

        Question question = questionList.get(currentQuestionIndex % questionList.size());
        return question;
    }

    public void addQuestion(Question question){
        questionList.add(question);
        // add question's score to maxScore
        maxScore+=question.getScore();
    }

    public void removeQuestion(int questionId){
        for(int i = 0; i < questionList.size(); i++){
            if(questionList.get(i).getId() == questionId){
                //deduct maxScore before remove
                maxScore-=questionList.get(i).getScore();
                questionList.remove(i);
            }
        }
    }

    public void answer(int choiceId){
        answerList.put(questionList.get(currentQuestionIndex % questionList.size()).getId(),choiceId);
    }

    public int checkScore(){
        int sum = 0;

        for(Map.Entry<Integer,Integer> entry : answerList.entrySet()){
            int questionId = entry.getKey();
            int answerId = entry.getValue();
            if(checkAnswer(questionId,answerId))
                sum += checkAnswer(questionId,answerId) ? getQuestionScoreById(questionId) : 0;
        }

        return sum;
    }

    public int getMaxScore() {
        return maxScore;
    }

    private boolean checkAnswer(int questionId, int answerId){
        try{
            return getChoiceById(getQuestionById(questionId).getChoices(),answerId).isCorrect();
        }
        catch (NullPointerException e){
            throw new RuntimeException("Question Id and Choice Id doesn't match");
        }
    }

    private int getQuestionScoreById(int questionId){
        return getQuestionById(questionId).getScore();
    }

    private Question getQuestionById(int questionId){
        for(int i = 0; i < questionList.size(); i++){
            if(questionList.get(i).getId() == questionId)
                return questionList.get(i);
        }
        throw new RuntimeException("There is no question id: "+questionId);
    }
    private Choice getChoiceById(List<Choice> choiceList,int choiceId){
        try{
            for(Choice c : choiceList){
                if(c.getId() == choiceId)
                    return c;
            }
        }
        catch (NullPointerException e){
            throw new RuntimeException("Choice list is null");
        }
        throw new RuntimeException("There is no choice id : "+choiceId);
    }


}
