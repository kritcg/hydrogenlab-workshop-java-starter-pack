package com.hydrogendev.lab.entity;

/**
 * Created by Chertpong on 17/1/2559.
 */
public class Choice {
    private int id;
    private String content;
    private boolean correct;

    public Choice(int id, String content, boolean correct) {
        this.id = id;
        this.content = content;
        this.correct = correct;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public boolean isCorrect() {
        return correct;
    }

    public void setCorrect(boolean correct) {
        this.correct = correct;
    }

}
