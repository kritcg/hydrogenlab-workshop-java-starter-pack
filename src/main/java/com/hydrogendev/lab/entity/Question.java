package com.hydrogendev.lab.entity;

import java.util.List;

/**
 * Created by Chertpong on 17/1/2559.
 */
public class Question {
    private int id;
    private String title;
    private String description;
    private List<Choice> choices;
    private int score;

    public Question(int id, String title, String description, List<Choice> choices, int score) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.choices = choices;
        this.score = score;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Choice> getChoices() {
        return choices;
    }

    public void setChoices(List<Choice> choices) {
        this.choices = choices;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
